/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connectdatabase.model;

/**
 *
 * @author nutty
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adisa
 */
public class User {
    private int id;
    private String password;
    private String gender;
    private String name;
    private int role;

    public User(int id, String password, String gender, String name, int role) {
        this.id = id;
        this.password = password;
        this.gender = gender;
        this.name = name;
        this.role = role;
    }

    public User(String password, String gender, String name, int role) {
        this.id = -1;
        this.password = password;
        this.gender = gender;
        this.name = name;
        this.role = role;
    }
    
    public User(){
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", password=" + password + ", gender=" + gender + ", name=" + name + ", role=" + role + '}';
    }
    
    public static User fromRS(ResultSet rs){
        User user = new User();
        try {
            user.setId(rs.getInt("user_id"));
            user.setName(rs.getString("user_name"));
            user.setGender(rs.getString("user_gender"));
            user.setPassword(rs.getString("user_password"));
            user.setRole(rs.getInt("user_role"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return user;
    }
    
}