/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connectdatabase;

/**
 *
 * @author nutty
 */
import com.mycompany.connectdatabase.dao.UserDao;
import com.mycompany.connectdatabase.helper.DatabaseHelper;
import com.mycompany.connectdatabase.model.User;

public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
//        User user1 = userDao.get(2);
//        System.out.println(user1);

//        User newUser = new User("pass", "F", "user3", 2);
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
//        insertedUser.setPassword("password");
//        user1.setGender("F");
//        userDao.update(user1);
//        User updateUser = userDao.get(user1.getId());
//        System.out.println(updateUser);
//        
//        userDao.delete(user1);
//        for(User u : userDao.getAll()){
//            System.out.println(u);
//        }
        for (User u : userDao.getAll(" user_name like 'u%' ", "user_name asc, user_gender desc")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
